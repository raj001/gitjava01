package web.tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class GoogleTest1 {

    private WebDriver driver;

    @BeforeTest()
    public void beforeTest(){

        WebDriverManager.chromedriver().setup();
        ChromeOptions options=new ChromeOptions();
        options.setHeadless(true);
        options.addArguments("window-size=1920,1080");
        driver=new ChromeDriver(options);
        driver.get("https://www.google.com/");
        driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
    }

    @Test(enabled = true)
    public void Test1() throws InterruptedException {
        driver.findElement(By.name("q")).sendKeys("gitlab cicd");
        Thread.sleep(4000);
    }

    @Test()
    public void Test2(){
        //mvn io.qameta.allure:allure-maven:serve
        System.out.println("example test");
    }


    @AfterTest()
    public void afterTest(){
        driver.quit();

    }

}
